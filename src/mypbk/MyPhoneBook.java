package mypbk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

public class MyPhoneBook {
	HashMap<String, String> phoneBooks;
	ArrayList<String> names;
	String selectedNumber;
	String selectedName;
	String phoneBookInfo[][] = { { "Mary Smith", "770-444-1101" }, { "John Simpson", "770-444-1102" }, { "William Solomon", "770-444-1103" } };

	public MyPhoneBook() {

		phoneBooks = new HashMap<String, String>();
		names = new ArrayList<String>();

		for (int i = 0; i < phoneBookInfo.length; i++) {
			phoneBooks.put(phoneBookInfo[i][0], phoneBookInfo[i][1]);
			names.add(phoneBookInfo[i][0]);
		}
		Collections.sort(names);
		selectedNumber = null;
		selectedNumber = null;
	}

	public static boolean equals(String l1, String l2) {
		return l1.equals(l2);
	}

	public Collection<String> getNames() {
		return names;
	}

	public void setSelectedName(String displayName) {
		this.selectedName = displayName;
		this.selectedNumber = (String) phoneBooks.get(displayName);
	}

	public String getSelectedNumber() {
		return selectedNumber;
	}

	public String getSelectedName() {
		return selectedName;
	}

}
