<%@ page isELIgnored = "false" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/functions.tld" prefix="f" %>

<html>
<head><title>PhoneBook</title></head>
<body bgcolor="white">

<jsp:useBean id="phoneBook" scope="application" class="mypbk.MyPhoneBook"/>

<form name="phoneBookForm" action="index.jsp" method="post">
<c:set var="selectedName" value="${param.names}" />
<c:set var="selectedFlag" value="${!empty selectedName}" />
<b>Name:</b>
<select name=names>
<c:forEach var="name" items="${phoneBook.names}" >
  <c:choose>
  	<c:when test="${selectedFlag}">
      <c:choose>
    		<c:when test="${f:equals(selectedName,name)}" >
      		<option selected>${name}</option>
    		</c:when>
    		<c:otherwise>
    			<option>${name}</option>
    		</c:otherwise>
      </c:choose>
  	</c:when>
  	<c:otherwise>
    	<option>${name}</option>
  	</c:otherwise>
  </c:choose>
</c:forEach>
</select>
<input type="submit" name="Submit" value="Submit">
</form>

<c:if test="${selectedFlag}" >
  <jsp:setProperty name="phoneBook" property="selectedName" value="${selectedName}" />
  <b>Phone Number: </b>${phoneBook.selectedNumber}
</c:if>

</body>
</html>